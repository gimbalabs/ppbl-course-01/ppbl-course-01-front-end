import Cardano from '../serialization-lib'
import {
    assetsToValue,
    createTxOutput,
    finalizeTx,
    initializeTx,
  } from "../transaction";
  import { fromHex } from "../../utils/converter";

// simple-tx

export const simpleTx = async ({ address, utxosParam, recAddr, memo, gimbals }) => {
    try {
        const { txBuilder, outputs } = initializeTx();
        const utxos = utxosParam.map((utxo) =>
            Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
        );
        const toAddr = recAddr

        outputs.add(
            createTxOutput(
                Cardano.Instance.Address.from_bech32(toAddr),
                assetsToValue([
                    { unit: "lovelace", quantity: "5000000" },
                    { unit: "cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059706c6179", quantity: gimbals}
                ])
            )
        )


        // just logging
        console.log("txBuilder", txBuilder)

        // Lets turn that memo into bonafide metadata

        // Then let's see about attaching gimbals to a transaction
        // How to deal with units on Gimbals (6 decimals)

        const txHash = await finalizeTx({
            txBuilder,
            utxos,
            outputs,
            changeAddress: address,
            metadata: memo
        })

        return {
            txHash,
        };
    }
    catch (error) {
        console.log(error, "in simpleTx")
    }


};
