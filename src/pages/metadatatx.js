import React, { useEffect, useState } from "react";
import { useStoreState } from "easy-peasy";
import { fromBech32, fromHex, toStr } from "../utils/converter";
import { Spinner, Flex, Heading, Text, Box, Button, FormControl, FormLabel, Input } from "@chakra-ui/react";
import { Formik, useFormik } from 'formik';

import { simpleTx } from "../cardano/simple-tx";

import useWallet from "../hooks/useWallet";
import {
    assetsToValue,
    createTxOutput,
    finalizeTx,
    initializeTx,
    serializeTxUnspentOutput,
    valueToAssets,
} from "../cardano/transaction";


// from connected wallet, get all UTXOs
function getWalletUtxoStrings(wallet) {
  const utxoStrings = wallet.utxos
      .map((utxo) => serializeTxUnspentOutput(utxo).output())
      .map((txOut) => valueToAssets(txOut.amount()))
  return [...new Set(utxoStrings)];
};

// given a hex-encoded Unit in a Value pair, return the policyID
function getPolicyId(unit) {
  let id = ""
  if(unit == "lovelace") {
      return ""
  }
  else {
      id = unit.slice(0,56)
  }
  return id
}

// given a hex-encoded Unit in a Value pair, return the asset name
function getTokenName(unit) {
  let name = ""
  if(unit == "lovelace") {
      return "ada"
  }
  else {
      let temp = fromHex(unit.substring(56))
      name = toStr(temp)
  }
  return name
}

function getQuantity(unit, quantity) {
  if(unit == "lovelace") {
      return quantity/1000000
  }
  return quantity
}

function getWalletAssetValues(wallet) {
  const utxoStrings = getWalletUtxoStrings(wallet);
  const assetValues = utxoStrings.map((utxoString) => utxoString.map((currentValue) => ({
      policy: getPolicyId(currentValue.unit),
      unit: getTokenName(currentValue.unit),
      quantity: getQuantity(currentValue.unit, currentValue.quantity)
  })))
  return [...new Set(assetValues)]
}

// Look for a particular asset in a wallet
function walletHoldsThisAsset(assetList, policyId, name) {
  let quantity = 0
  assetList.map((asset) => {
      if ((asset.policy === policyId) && (asset.unit === name)){
          console.log("assets", asset.policy, "name", asset.unit)
          quantity = asset.quantity
      }
  })

  return quantity
}

const MetadataTx = () => {
  const connected = useStoreState((state) => state.connection.connected);
  const [walletAddress, setWalletAddress] = useState(null);
  const [walletBalance, setWalletBalance] = useState("");
  const [walletAssets, setWalletAssets] = useState([]);
  const [walletUtxos, setWalletUtxos] = useState([]);
  const { wallet } = useWallet(null);
  const [loading, setLoading] = useState(true);

  const [playTokens, setPlayTokens] = useState(0);

  // on loading change, if wallet is connected, set wallet address
  useEffect(async () => {
    if (connected && wallet) {
      setWalletBalance(wallet.balance);
      setWalletAddress(wallet.address);
      setWalletAssets(getWalletAssetValues(wallet));
    }
  }, [loading]);

  useEffect(async () => {
    if (connected && wallet) {
      const myUtxos = await wallet.utxos;
      setWalletUtxos(wallet.utxos);
      setLoading(false);
    }
  }, [wallet]);

  useEffect(() => {
    if (connected && wallet){
      setPlayTokens(walletHoldsThisAsset(walletAssets[0], "cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059", "play"))
    }
  }, [walletAssets])

  useEffect(() => {
    if (walletUtxos.length == 0) {
      setLoading(true);
    }
    if (walletAddress == null || walletAddress == "") {
      setLoading(true);
    }
    if (loading) {
      setLoading(false);
    }
  }, [walletUtxos, setWalletAddress]);

  const sendMyTransaction = async () => {
    const fromMyWallet = {
      "address": fromBech32(walletAddress),
      "utxosParam": walletUtxos,
      "recAddr": recAddr,
      "memo": metadataMsg,
      "gimbals": playToSend,
    }
    const txHash = await simpleTx(fromMyWallet)
    setLoading(true)
  }

  const formik = useFormik({
    initialValues: {
      receivingAddress: "",
      metadataMessage: "",
    },
  })

  const [recAddr, setRecAddr] = useState(formik.receivingAddress)
  const [metadataMsg, setMetadataMsg] = useState(formik.metadataMessage)
  const [playToSend, setPlayToSend] = useState(formik.numPlayTokensToSend)


  useEffect(() => {
    const a = formik.values.receivingAddress;
    setRecAddr(a);
  }, [formik.values.receivingAddress])

  useEffect(() => {
    const a = formik.values.metadataMessage;
    setMetadataMsg(a);
  }, [formik.values.metadataMessage])

  useEffect(() => {
    const a = formik.values.numPlayTokensToSend;
    setPlayToSend(a);
  }, [formik.values.numPlayTokensToSend])

  return (
    <>
      <title>demo v0</title>
      <Flex
        w="100%"
        mx="auto"
        direction="column"
        wrap="wrap"
        bg="gl-yellow"
        p="10"
      >
        <Box w="50%" mx="auto" my='5'>
          <Heading size="4xl" color="gl-blue" fontWeight="medium" py="5">
            The button below sends a simple transaction
          </Heading>
          <Text p='1' color="gl-red" fontSize='sm'>Balance: {walletBalance} tAda</Text>
          <Text p='1' color="gl-red" fontSize='sm'>Play Balance: {walletBalance} tAda</Text>
          <Text p='1' color="gl-red" fontSize='sm'>Play Tokens: {playTokens}</Text>
          <Text p='1' color="gl-red" fontSize='sm'>Connected Address: {walletAddress}</Text>
        </Box>
        <Box w="50%" p='5' mx="auto" bg="gl-blue" color='white'>
          <FormControl>
            <FormLabel fontWeight="bold">
              Enter Receive Address
            </FormLabel>
            <Input name="receivingAddress" onChange={formik.handleChange} value={formik.values.receivingAddress} />
            <FormLabel pt='5' fontWeight="bold">
              Add Gimbal Distribution Memo:
            </FormLabel>
            <Input name="metadataMessage" onChange={formik.handleChange} value={formik.values.metadataMessage} />
            <FormLabel pt='5' fontWeight="bold">
              Number of Play to Send:
            </FormLabel>
            <Input name="numPlayTokensToSend" onChange={formik.handleChange} value={formik.values.numPlayTokensToSend} />
          </FormControl>
          <Box mt='5'>
            <Text p='1' fontSize='sm'>Send Gimbals to: {recAddr}</Text>
            <Text p='1' fontSize='sm'>Gimbal Distribution Memo: {metadataMsg}</Text>
            <Text p='1' fontSize='sm'>Number of Play Tokens: {playToSend}</Text>
            <Button m='3' color='gl-blue' onClick={sendMyTransaction}>Send TX</Button>
          </Box>
        </Box>
      </Flex>
    </>
  );
};

export default MetadataTx;
